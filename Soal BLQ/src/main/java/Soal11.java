import java.util.Scanner;

public class Soal11 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        String word = scanner.nextLine();
        String[] arrWord = word.split("");
        int sizeOf = arrWord.length;

        for (int i = sizeOf-1; i >= 0 ; i--) {
            int stars = sizeOf/2;
            for (int j = 0; j < stars; j++) {
                System.out.print("*");
            }
            System.out.print(arrWord[i]);

            for (int j = 0; j < stars; j++) {
                System.out.print("*");
            }
            System.out.println();
        }

    }
}
