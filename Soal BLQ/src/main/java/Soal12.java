import java.util.Arrays;

public class Soal12 {

    public static void main(String[] args) {
        int[] arrRandom = {1,2,1,3,4,7,1,1,5,6,1,8};

        for (int i = 0; i < arrRandom.length-2; i++) {
            for (int j = i; j < arrRandom.length-1; j++) {
                if (arrRandom[i] > arrRandom[j]){
                    int temp;
                    temp = arrRandom[i];
                    arrRandom[i] = arrRandom[j];
                    arrRandom[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(arrRandom));
    }

}
