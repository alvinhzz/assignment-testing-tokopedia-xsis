import java.util.Scanner;

public class Soal13 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("jam : ");
        String jam = scanner.nextLine();

        String[] arrJam = jam.split(":");

        int H = 30 * Integer.parseInt(arrJam[0]);
        int M = (11 * Integer.parseInt(arrJam[1]))/2;

        int sudut = Math.abs(H-M);
        System.out.println("Jam " + jam + " | " + sudut + " derajat");


    }
}
