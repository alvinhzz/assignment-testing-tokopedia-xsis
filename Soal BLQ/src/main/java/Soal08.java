public class Soal08 {
    public static void main(String[] args) {

        int[] arrDeret = {1,2,4,7,8,6,9};
        int current_sum = 0;
        int max_sum = 0;
        int min_sum = 0;

        for (int i = 0; i < 4; i++) {
            current_sum = arrDeret[i] + arrDeret[i+1] + arrDeret[i+2] + arrDeret[i+3];

            if (i==0){
                min_sum = current_sum;
            }

            if (current_sum > max_sum){
                max_sum = current_sum;
            } else if (current_sum < min_sum) {
                min_sum = current_sum;
            }
        }

        System.out.println("nilai minimal : " + min_sum);
        System.out.println("nilai maximal : " + max_sum);

    }
}
