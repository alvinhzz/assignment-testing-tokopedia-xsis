import java.util.Scanner;

public class Soal15 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String time12 = scanner.nextLine();

        String time = null;
        String[] arrTime12 = time12.split(" ");
        String timeForConvert = arrTime12[0];
        String timeFormat12 = arrTime12[1];
        String[] arrTimeForConvert = timeForConvert.split(":");
        int H = Integer.parseInt(arrTimeForConvert[0]);
        int M = Integer.parseInt(arrTimeForConvert[1]);
        int S = Integer.parseInt(arrTimeForConvert[2]);

        if (timeFormat12.equalsIgnoreCase("pm")){
            if (H == 12){
                time = "00" + ":" + M + ":" + S;
            }else{
                time = (H+12) + ":" + M + ":" + S;
            }
        } else if (timeFormat12.equalsIgnoreCase("am")) {
            time = H + ":" + M + ":" + S;
        }else {
            System.out.println("time format wrong");
            System.exit(0);
        }

        System.out.println(time);
    }
}
