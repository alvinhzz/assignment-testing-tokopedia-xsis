import java.util.Arrays;
import java.util.Scanner;

public class Soal01 {
    public static void main(String[] args) {
        int[]  kacamata = {500, 600, 700, 800},
                baju = {200, 400, 350},
                sepatu = {400, 350, 200, 300},
                buku = {100, 50, 150};

        int[][] barang = {kacamata, baju, sepatu, buku};

        int item_dibeli = 0;
        String[] nama_barang = {"kaca_mata", "baju", "sepatu", "buku"};
        String hasil = "";

        Scanner scanner = new Scanner(System.in);
        System.out.print("Jumlah uang: ");
        int jumlah_uang = scanner.nextInt();

        for (int i = 0; i < barang.length; i++) {
            for (int j = 0; j < barang[i].length; j++) {

                if (barang[i][j] <= jumlah_uang){
                    jumlah_uang = jumlah_uang-barang[i][j];
                    item_dibeli++;

                    if (i == barang.length-1){
                        hasil += nama_barang [i]+ " " + barang[i][j];
                    }else {
                        hasil +=  nama_barang[i] + " " + barang[i][j] + ", ";
                    }
                }

            }
        }
        System.out.print("Jumlah item yang bisa dibeli: " + item_dibeli + " (" + hasil + ")");
    }
}
