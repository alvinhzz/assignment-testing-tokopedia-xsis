import java.util.Arrays;
import java.util.Scanner;

public class Soal09 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("n : ");
        int n = scanner.nextInt();

        int[] deret = new int[n];

        for (int i = 0; i < n; i++) {
            if (i==0){
                deret[i] = n;
            }else {
                deret[i] = deret[i-1] + n;
            }
        }

        System.out.println("N = " + n + " " + Arrays.toString(deret));

    }

}
