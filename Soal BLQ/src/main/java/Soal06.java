import java.util.Scanner;

public class Soal06 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();

        String[] arrWord = word.split("");
        boolean isPalindrome = true;
        int length = arrWord.length-1;
        for (int i = 0; i < length; i++) {
                if (!arrWord[i].equalsIgnoreCase(arrWord[length-i])) {
                    isPalindrome = false;
                    System.out.println("not palindrome");
                    break;
                }
        }

        if (isPalindrome == true){
            System.out.println("is palindrome");
        }
    }
}
