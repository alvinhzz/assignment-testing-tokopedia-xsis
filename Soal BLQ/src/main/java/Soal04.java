import java.util.Arrays;
import java.util.Scanner;

public class Soal04 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("n: ");
        int n = scanner.nextInt();

        int[] primeNumber = new int[n];
        int candidate = 1;

        for (int i = 0; i < n;) {
            int isPrime = 0;
            for (int j = 1; j <= candidate; j++) {
                if (candidate%j == 0){
                    isPrime++;
                }
            }

            if (isPrime == 2){
                primeNumber[i] = candidate;
                i++;
            }
            candidate++;
        }

        System.out.println(Arrays.toString(primeNumber));
    }
}
