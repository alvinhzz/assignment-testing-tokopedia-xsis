import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal03 {
    public static void main(String[] args) throws ParseException {
        int detik = 1000;
        int menit = 60 * detik;
        int jam = 60 * menit;

        String jam_masuk;
        String jam_keluar;
        int tarif;

        Scanner scanner = new Scanner(System.in);

        System.out.print("masuk: ");
        jam_masuk = scanner.nextLine();
        System.out.print("keluar: ");
        jam_keluar = scanner.nextLine();

        Locale locale = new Locale("id", "ID");
        SimpleDateFormat sdf =new SimpleDateFormat("dd MMMM yyyy | HH:mm:ss", locale);
        Date checkIn = sdf.parse(jam_masuk);
        Date checkOut = sdf.parse(jam_keluar);

        long milis_in = checkIn.getTime();
        long milis_out = checkOut.getTime();
        long milis_totals = milis_out-milis_in;

        int durasiParkir = (int) (milis_totals/jam);

        if (durasiParkir > 0 && durasiParkir <= 8){
            tarif = durasiParkir*1000;
            System.out.println("Durasi: " + durasiParkir + " jam");
            System.out.print("Tarif parkir: " + tarif);
        } else if (durasiParkir > 8 && durasiParkir <= 24) {
            tarif = 8000;
            System.out.println("Durasi: " + durasiParkir + " jam");
            System.out.print("Tarif parkir: " + tarif);
        } else if (durasiParkir > 24) {
            tarif = 15000 + ((durasiParkir-24)*1000);
            System.out.println("Durasi: " + durasiParkir + " jam");
            System.out.print("Tarif parkir: " + tarif);
        }else if (durasiParkir == 0){
            System.out.println("Durasi: < 1 jam");
            System.out.print("Tarif parkir: 1000");
        }else {
            System.out.println("System can't resolve.");
        }

    }
}
