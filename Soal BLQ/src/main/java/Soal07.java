import java.util.Arrays;

public class Soal07 {
    public static void main(String[] args) {

        int[] arrDeretAngka = {8,7,0,2,7,1,7,6,3,0,7,1,3,4,6,1,6,4,3};
        int size_of = arrDeretAngka.length;

        //mean
        int sum = 0;
        for (int i = 0; i < size_of; i++) {
            sum += arrDeretAngka[i];
        }
        double mean = (double) sum /size_of;
        System.out.println("mean : " + mean);

        //median
        Arrays.sort(arrDeretAngka);
        int median;

        if(size_of%2 == 0){
            median = size_of/2;
            System.out.println("median : " + arrDeretAngka[median]);
        }else {
            median = (size_of+1)/2;
            System.out.println("median : " + arrDeretAngka[median]);
        }

        //modus
        int jumlah_angka = 0;
        int jumlah_sekarang = 0;
        int modus = arrDeretAngka[0];
        int angka_sekarang = arrDeretAngka[0];

        for (int i = 0; i < size_of; i++) {
            if (arrDeretAngka[i] == angka_sekarang){
                jumlah_sekarang++;
            }else {
                jumlah_sekarang = 1;
                angka_sekarang = arrDeretAngka[i];
            }

            if (jumlah_sekarang > jumlah_angka || (jumlah_sekarang == jumlah_angka && angka_sekarang < modus)){
                jumlah_angka = jumlah_sekarang;
                modus = angka_sekarang;
            }
        }

        System.out.println("modus : " + modus);
    }
}
