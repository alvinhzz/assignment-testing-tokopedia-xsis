import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class Soal02 {
    public static void main(String[] args) throws ParseException {
        int detik = 1000;
        int menit = 60 * detik;
        int jam = 60 * menit;
        int hari = 24 * jam;

        int[] book = {14, 3, 7, 7};
        String[] book_name = {"A", "B", "C", "D"};

        Scanner scanner = new Scanner(System.in);
        System.out.print("Tanggal pinjam: ");
        String tgl_pinjam = scanner.nextLine();
        System.out.print("Tanggal kembali: ");
        String tgl_kembali = scanner.nextLine();

        Locale locale = new Locale("id", "ID");
        SimpleDateFormat sdf =new SimpleDateFormat("dd MMMM yyyy", locale);
        Date date_out = sdf.parse(tgl_pinjam);
        Date date_in = sdf.parse(tgl_kembali);

        long milis_out = date_out.getTime();
        long milis_in = date_in.getTime();

        int milis_totals = (int) ((milis_in - milis_out)/hari);

        System.out.println("\n== DENDA ==");
        for (int i = 0; i < book.length; i++){
            int denda;
            int terlambat = milis_totals-book[i];

            if (terlambat < 1){
                denda = 0;
                System.out.println("Buku " + book_name[i] + ": " + denda + " || terlambat 0 hari");
                continue;
            }

            denda = terlambat*100;
            System.out.println("Buku " + book_name[i] + ": " + denda + " || terlambat " + terlambat + " hari");
        }

    }
}
