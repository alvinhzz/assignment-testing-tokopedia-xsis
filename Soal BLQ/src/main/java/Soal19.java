import java.util.Scanner;

public class Soal19 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("words : ");
        String words = scanner.nextLine();
        String alphabet = "abcdefghijklmnopqrstuvwxyz";
        String[] arrAlphabet = alphabet.split(  "");
        boolean isPangram = true;

        for (int i = 0; i < arrAlphabet.length; i++) {
            if (!words.contains(arrAlphabet[i].toLowerCase())){
                isPangram = false;
                break;
            }
        }

        if (isPangram){
            System.out.println("it is Pangram");
        }else {
            System.out.println("is not Pangram");
        }
    }
}
