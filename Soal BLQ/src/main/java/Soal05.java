import java.util.Arrays;
import java.util.Scanner;

public class Soal05 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("n: ");
        int n = scanner.nextInt();

        int[] fibonacci = new int[n];
        for (int i = 0; i < n; i++) {
            if (i == 0 || i == 1){
                fibonacci[i] = 1;
                continue;
            }

            fibonacci[i] = fibonacci[i-1] + fibonacci[i-2];
        }

        System.out.println(Arrays.toString(fibonacci));
    }
}
