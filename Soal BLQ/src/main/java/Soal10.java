import java.util.Scanner;

public class Soal10 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        String[] arrSplit = input.split(" ");
        String output = "";

        for (int i = 0; i < arrSplit.length; i++) {
            String word = "";
            for (int j = 0; j < arrSplit[i].length(); j++) {
                String[] arrWord = arrSplit[i].split("");
                word = arrWord[0] + "***" + arrWord[arrWord.length-1];
            }
            output += word + " ";
        }

        System.out.println(output);
    }
}
